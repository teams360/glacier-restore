#!/bin/bash
PATH=/bin:/usr/bin:/usr/local/bin

usage()
{
cat << EOF
usage: $0 <CUST_ID> <LOG_DATE>

CUST_ID  - Customer ID  
LOG_DATE - Date for LOG Files to Restore to S3 Storage. Enter in YYYY-MM-DD format

OPTIONS:
   -h            Show this message
EOF
}


while getopts "h" OPTION
do
  case "$OPTION" in
        h) usage; exit;;
        ?) usage;  exit 1;;
  esac
done

shift $(($OPTIND - 1))

CUST_ID=$1  usage && exit 1
fi

if [ -z $LOG_DATE ]; then
  usage && exit 1
fi


LC_CUST_ID=`echo ${CUST_ID} | tr '[:upper:]' '[:lower:]'`

aws s3 ls s3://prologic-${LC_CUST_ID}-logs/${LOG_DATE}/ | awk '{if ($4) print $4}' > ${LC_CUST_ID}_logfiles_${LOG_DATE}.txt
for x in `cat ${LC_CUST_ID}_logfiles_${LOG_DATE}.txt`
do
    echo "restoring $x"
    aws s3api restore-object --bucket prologic-${LC_CUST_ID}-logs --key "${LOG_DATE}/$x" --restore-request '{"Days":7}'
done
LOG_DATE=$2


if [ -z $CUST_ID ]; then
