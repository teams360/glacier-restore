#!/bin/bash
PATH=/bin:/usr/bin:/usr/local/bin

aws s3 ls s3://prologic-carr-teams/tomcat-01/ | grep server.log | awk '{if ($4) print $4}' > tomcat-01.txt
for x in `cat tomcat-01.txt`
do
    echo "restoring $x"
    aws s3api restore-object --bucket prologic-carr-teams --key "tomcat-01/$x" --restore-request '{"Days":14}'
done

